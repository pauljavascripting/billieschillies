module.exports = {
  siteMetadata: {
    title: 'BilliesChillies',
  },
  plugins: ['gatsby-plugin-react-helmet', 'gatsby-plugin-less'],
}
