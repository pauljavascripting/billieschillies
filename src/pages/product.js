import React, {Component} from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { connect } from "react-redux"
import Layout from '../components/layout'
import { Grid, Divider, Button, Image } from 'semantic-ui-react'

let _this

// Cart.propTypes = {
//   count: PropTypes.number.isRequired,
//   increment: PropTypes.func.isRequired,
// }

const mapStateToProps = ({ count, byHash }) => {
  return { count, byHash }
}

const mapDispatchToProps = dispatch => {
  return { 

    increment: () => dispatch({ type: `INCREMENT` }),

    quantityUpdated:  (id, qty, action) => dispatch({ type: 'QTY_UPDATED', id:id, qty:qty, action: action })

   }
}

class DefaultLayout extends React.Component {

  constructor(){

    super()

    _this = this

  }

  render() {

    return (
     
     <Layout>
        
         <ConnectedCart  />
        
     </Layout>

    )
  }
}



const Cart = ({ count, byHash, quantityUpdated, total=0, qty=0, selectedID }) => (

   <Grid columns={4}>

    { _this.props.location.state!=null &&

        Object.keys(byHash).map((key) => {
            const product = byHash[key];
            
            if(byHash[key].id==_this.props.location.state.id ){

              return (
                
                <div className='content' key={key}><Image src={product.img} size='small' floated='left' /><h2>{product.name}</h2>{product.desc}<br /><b>Price:£{product.price}, Quantity:{product.qty}</b><br /><button color='red' onClick={ ()=>quantityUpdated(byHash[key].id, byHash[key].qty+1, 'more')}>+</button><button color='red' onClick={ ()=>quantityUpdated(byHash[key].id, byHash[key].qty-1, 'less')}>-</button></div>
                
              )

            }
           
        })
    }

 


</Grid>

)

const ConnectedCart = connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart)

export default DefaultLayout