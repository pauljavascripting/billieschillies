import React, { Component } from 'react'
import { Grid, Modal, Visibility, Button, Image, Header, Divider } from 'semantic-ui-react'
import Layout from '../components/layout'
import LazyImage from '../components/lazyimage'
import { connect } from "react-redux"
import { Link } from 'gatsby'

const axios = require('axios') // fetch doesn't work on iphone!

/*
* NB You cannot display dynamic modal variables in the render()
* You must store the values for the selected modal in the state object instead
*
* Redux:
* https://github.com/gatsbyjs/gatsby/tree/master/examples/using-redux
*
* Pagination:
* Semantic UI
*
* Scaling & positioning of the product images is done by checkScreenSize() not by CSS
* 
* Data is loaded & then stored in a redux object called byHash
* A state array is then used to store the byHash object & display the product information
* Clicking the 'BUY NOW' button updates the redux store.....
* The redux store is also updated via the Cart page
*
*/

class IndexPage extends Component {

  constructor(){

    super();

    this.state = {dataLoaded:false, array:null,  open:false, modalId:null, modalTitle:'', modalImage:null, modalPrice:'', modalDesc:'', columnNum:1, contextRef:null, calculations: {
      direction: 'none',
      height: 0,
      width: 800,
      topPassed: false,
      bottomPassed: false,
      pixelsPassed: 0,
      percentagePassed: 0,
      topVisible: false,
      bottomVisible: false,
      fits: false,
      passing: false,
      onScreen: false,
      offScreen: false,
    }}

   // this.showModal = this.showModal.bind(this);
   // this.closeModal = this.closeModal.bind(this);
    this.checkScreenSize = this.checkScreenSize.bind(this);
    this.handleContextRef = this.handleContextRef.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.increaseQty = this.increaseQty.bind(this);

  }

  // showModal(id){

  //   // change this below--------V
  //   let shopArray = [];
  //     for (var key in this.props.byHash) {
  //       shopArray.push(this.props.byHash[key]);
  //     }

  //   // get relevant row & update qty
  //   let shopObj = shopArray.find(obj => obj.id == id);

  //   this.setState({
  //      open: true,
  //      modalId: shopObj.id,
  //      modalTitle: shopObj.name,
  //      modalImage: shopObj.img,
  //      modalPrice: shopObj.price,
  //      modalDesc:  shopObj.desc,
  //      modalQty:  shopObj.qty,
  //   });

  //   // clear array
  //   shopArray = null;

  // }

  // closeModal(){

  //   this.setState({
  //      open: false
  //   });

  // }

  increaseQty(id, qty){
   
    qty++

    this.props.quantityUpdated(id, qty, 'more');

    //this.closeModal()

  }

 componentDidMount(){

    let self = this;
    let shopDataObject = [];
    let storeByHashLength = Object.keys(this.props.byHash).length
   
    // 1a. load data on first visit ----------------------------------------------------------------------    
    if(storeByHashLength<=0){
    
     axios.get('https://javascripting.uk/javascriptingwp/?rest_route=/mynamespace/v1/billieschilliesproducts/')
     .then(function (response) {

        // update state
        self.setState({dataLoaded:true})
      
        // store loaded data ------------------------------
        let imgs = response.data.map((row, i) => {

          if(row.src){

            let idArray = row.ids.split(',')

            row.src.map((imgId, i) =>

               shopDataObject.push({id:idArray[i], qty:0, name:response.data[1][i], price:response.data[3][i], img:imgId, desc:response.data[4][i]})

            );
            
          }

       })
        // end store loaded data ------------------------------

        // store data in redux
        const shopData = Object.assign({}, shopDataObject);
        self.props.addItemsToShop(shopData)
        
        // clear
        shopDataObject = null
         
     })
    .catch(function (error) {
       console.log(error);
    });

    }
    else{

      // 1b. add redux data into state & display content ----------------------------------------------------------------------   
      
      // update state
      self.setState({dataLoaded:true})
     
    }

    // check for screen resize --------------------------
    window.addEventListener("resize", this.checkScreenSize);
    this.checkScreenSize()

  }

  handleContextRef(contextRef){

    this.setState({ contextRef: contextRef })

  }

  handleUpdate(e, calculations){

    this.setState({ calculations: calculations })

  }

  checkScreenSize(){

    let columnNum = 0;
    let screenSize = window.screen.width;

    switch(true){

        case(screenSize<500): // iphone

          columnNum = 2

        break;
        
        case(screenSize>500 && screenSize<800): // ipad

          columnNum = 4
        
        break;

        case(screenSize>800): // computer

          columnNum = 5
        
        break;
        default:

          columnNum = 3

        break;

    }

     this.setState({
       columnNum: columnNum
     });

  }
  
  render(){
    
    let imgs;
    //const { calculations, contextRef } = this.state; - causing leak????
    
    let self = this
    if(self.state.dataLoaded){

      imgs = Object.keys(self.props.byHash).map(function(s){ return <Grid.Column key={s}>{

          
          <Grid padded><Grid.Row><div className='productImg'><Link to="/product/" activeStyle={{color: "red",}} state={{id:self.props.byHash[s].id }} ><Image src={self.props.byHash[s].img} /></Link></div></Grid.Row><Grid.Row className='productRow'>{self.props.byHash[s].name}</Grid.Row><Grid.Row>£{self.props.byHash[s].price}</Grid.Row><Grid.Row><Button onClick={ ()=>self.increaseQty(self.props.byHash[s].id, self.props.byHash[s].qty)} color='red'>Buy</Button></Grid.Row></Grid>


        }</Grid.Column> })

    }
    else{

        imgs = <div className="ui active centered inline loader red"></div>
    }

      return(
       
         <Layout>

               

                <Grid container columns={this.state.columnNum} >
                 
                   {imgs}

                </Grid>

        </Layout>

      )

  }

}

const mapStateToProps = ({ count, byHash }) => {
  return { count, byHash }
}

const mapDispatchToProps = dispatch => {
  return { 

      increment:   () => dispatch({ type: 'INCREMENT' }), // original

      addItemsToShop: (object) => dispatch({ type: 'ADD_ITEMS_TO_SHOP', object }), // add items into redux

      quantityUpdated:  (id, qty, action) => dispatch({ type: 'QTY_UPDATED', id:id, qty:qty, action:action })

  }
  
}

// Connect Redux to App ---------------------------------------------------------------------------------- //
export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);