import React from 'react'

import Layout from '../components/layout'

const ContactPage = () => (
  <Layout>
    <h2>Contact Us</h2>
    <p>Feel free to get in touch for any reason.</p>
    <p>
	Write to us at:
	<br />
	Billies Chillies Ltd, Hot House, Smith Street, Nottingham, NG3 1JH
	</p>
	<p>
	Tel: 0115 9413223
	</p>
	<p>
	Email: hello@billieschillies.co.uk
	</p>
  </Layout>
)

export default ContactPage
