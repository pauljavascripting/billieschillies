import React, {Component} from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { connect } from "react-redux"
import Layout from '../components/layout'
import { Grid, Divider, Button } from 'semantic-ui-react'

const Cart = ({ count, byHash, quantityUpdated, total=0, qty=0 }) => (

   <Grid columns={4}>

       { 

        Object.keys(byHash).map((key) => {
            const product = byHash[key];

            total += (product.qty * product.price)
            qty += product.qty

            if(byHash[key].qty>0){

              return (
                

                <Grid.Row className='cartRow' key={key}><Grid.Column><img src={product.img} /></Grid.Column><Grid.Column verticalAlign='middle' width='8'><h3>{product.name} x {product.qty} @ £{product.price}</h3></Grid.Column><Grid.Column verticalAlign='middle' width='4' align='right'>{<button onClick={ ()=>quantityUpdated(byHash[key].id, byHash[key].qty+1, 'more')}>+</button>}{<button onClick={ ()=>quantityUpdated(byHash[key].id, byHash[key].qty-1, 'less')}>-</button>}</Grid.Column></Grid.Row>

                //<Grid.Row className='cartRow' key={key}><Grid.Column><img src={product.img} /></Grid.Column><Grid.Column verticalAlign='middle' ><h3>{product.name} x {product.qty}</h3></Grid.Column><Grid.Column verticalAlign='middle'><h3>£{product.price}</h3></Grid.Column><Grid.Column verticalAlign='middle'>{<button onClick={ ()=>quantityUpdated(byHash[key].id, byHash[key].qty+1, 'more')}>+</button>}{<button onClick={ ()=>quantityUpdated(byHash[key].id, byHash[key].qty-1, 'less')}>-</button>}</Grid.Column></Grid.Row>
                
              )

            }
          

        })
      }

      {  total == 0  && 
      
        <Grid.Row><Grid.Column width='16'>Your cart is currently empty. Check out the <Link to="/">homepage</Link> to start shopping!</Grid.Column></Grid.Row>

      }

      {  total > 0  && 
      
        <div>
        <Grid.Row className='paddedRow'><Grid.Column><h2>Total:£{total}</h2></Grid.Column></Grid.Row>
        <Grid.Row><Grid.Column><Button size='tiny' color='red'>Pay</Button></Grid.Column></Grid.Row>
        </div>

      }

</Grid>

)

// Cart.propTypes = {
//   count: PropTypes.number.isRequired,
//   increment: PropTypes.func.isRequired,
// }

const mapStateToProps = ({ count, byHash }) => {
  return { count, byHash }
}

const mapDispatchToProps = dispatch => {
  return { 

    increment: () => dispatch({ type: `INCREMENT` }),

    quantityUpdated:  (id, qty, action) => dispatch({ type: 'QTY_UPDATED', id:id, qty:qty, action: action })

   }
}

class DefaultLayout extends React.Component {

  render() {

    return (
     
     <Layout>
        <h2>Shopping Cart</h2>

         <ConnectedCart />
        
     </Layout>

    )
  }
}

const ConnectedCart = connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart)

export default DefaultLayout