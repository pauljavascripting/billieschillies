import React, { Component } from 'react'
import { Grid, product, Visibility, Button, Image, Header, Divider } from 'semantic-ui-react'
import Layout from '../components/layout'
import LazyImage from '../components/lazyimage'
import { connect } from "react-redux"
import { Link } from 'gatsby'

const axios = require('axios') // fetch doesn't work on iphone!

/*
* NB You cannot display dynamic product variables in the render()
* You must store the values for the selected product in the state object instead
*
* Redux:
* https://github.com/gatsbyjs/gatsby/tree/master/examples/using-redux
*
* Pagination:
* Semantic UI
*
* Scaling & positioning of the product images is done by checkScreenSize() not by CSS
* 
* Data is loaded & then stored in a redux object called byHash
* A state array is then used to store the byHash object & display the product information
* Clicking the 'BUY NOW' button updates the redux store.....
* The redux store is also updated via the Cart page
*
*/

class IndexPage extends Component {

  constructor(){

    super();

    this.state = {productId:0,  productTitle:'', productImage:'', productPrice:'', productDesc:'', productQty:'' }

  }

 componentDidMount(){

    // store id
    if(this.props.byHash[0]){
      const id = this.props.location.state.id

      // get product
      let shopArray = [];
        for (var key in this.props.byHash) {
          shopArray.push(this.props.byHash[key]);
        }

      // get relevant row & update qty
      let shopObj = shopArray.find(obj => obj.id == id);

      this.setState({
         productId: shopObj.id,
         productTitle: shopObj.name,
         productImage: shopObj.img,
         productPrice: shopObj.price,
         productDesc:  shopObj.desc,
         productQty:  shopObj.qty,
      });

      // clear array
      shopArray = null;

    }
    
  }

  quantityUpdated(id, qty, action){

    // the main class needs to intrinsically linked to the store object
    
    this.props.quantityUpdated(id, qty, action)

    console.log('qty:'+qty)

  }

  render(){ 

    let content
    
    if(this.state.productDesc){

      content = <div><h2>{this.state.productTitle}</h2><Image src={this.state.productImage} size='small' floated='left' />{this.state.productDesc}x{this.state.productQty}<br /><button onClick={ ()=>this.quantityUpdated(this.state.productId, this.state.productQty+1, 'more')}>+</button><button onClick={ ()=>this.quantityUpdated(this.state.productId, this.state.productQty-1, 'less')}>-</button><br /></div>

    }
    else{

      content = <div>No product selected. Please visit the <Link to="/">homepage</Link> to begin!</div>
    }
    
    
      return(
       
         <Layout>

               {content}

        </Layout>

      )

  }

}

const mapStateToProps = ({ count, byHash }) => {
  return { count, byHash }
}

const mapDispatchToProps = dispatch => {
  return { 

     
      quantityUpdated:  (id, qty, action) => dispatch({ type: 'QTY_UPDATED', id:id, qty:qty, action:action })

  }
  
}

// Connect Redux to App ---------------------------------------------------------------------------------- //
export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);