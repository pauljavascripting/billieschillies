import React from 'react'

import Layout from '../components/layout'

const AboutPage = () => (
  <Layout>
    <h2>About Billies Chillies</h2>
    <p>We have always loved chill sauce, but why cover nice food with poor quality condiments? Dissatisfied with the gloopy and bland sauces, we decided to start collecting the best around.
	Billies Chillies started out with as a market stall on our local farmer’s market & eventually led to full-time business.</p>
	<p>
	Since then we’ve spread our sauces onto tables across the country and even abroad. What started as a hobby has turned into a full time mission to change what goes on the nation’s plates. We hope you find your dream chilli sauce right here!
	</p>
	<p>
	Website by <a target='_blank' href='http://javascripting.uk'>Javascripting</a>
	</p>

  </Layout>
)

export default AboutPage
