import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'
import { Container, Grid, Menu } from 'semantic-ui-react'
import Header from './header'
import 'semantic-ui-less/semantic.less'
import { Link } from 'gatsby'
import { connect } from "react-redux"

const LinkedItem = ({ children, ...props }) => (
  <Menu.Item as={Link} activeClassName='active' {...props}>{children}</Menu.Item>
)

const Layout = ({ children, data }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            { name: 'description', content: 'Sample' },
            { name: 'keywords', content: 'sample, something' },
          ]}
        />

        <Container>
       
          <Grid padded>
           
            <Grid.Column mobile={16} tablet={16} computer={16} >
               
              <Menu stackable>
                <Menu.Item>
                  <Link to="/"><div className='logo'></div></Link>
                </Menu.Item>

                <Menu.Item position='right' name='About'><Link to="/about" activeClassName="active">About</Link></Menu.Item>

                <Menu.Item name='contact'><Link to="/contact" activeClassName="active">Contact</Link></Menu.Item>

                <Menu.Item name='cart'><Link to="/cart" activeClassName="active">Cart <ConnectedCart /></Link></Menu.Item>
              </Menu>

            </Grid.Column>    
           
            <Grid.Column mobile={16} tablet={16} computer={16}>
              
              {children}
              
            </Grid.Column>
            
          </Grid>
        </Container>
      </>
    )}
  />
)

//  Removed from Layout()
//  <Header siteTitle={data.site.siteMetadata.title} />
// <LinkedItem to='/page-2'>About</LinkedItem>

// Layout.propTypes = {
//   children: PropTypes.node.isRequired,
// }

const Counter = ({ count }) => (

  <span>({count})</span>

)

const mapStateToProps = ({ count }) => {
  return { count }
}

const ConnectedCart = connect(
  mapStateToProps
)(Counter)

export default Layout
