import { createStore as reduxCreateStore } from "redux"

/*
*	https://hackernoon.com/redux-patterns-add-edit-remove-objects-in-an-array-6ee70cab2456
* https://github.com/gatsbyjs/gatsby/tree/master/examples/using-redux
* https://stackoverflow.com/questions/49604096/react-redux-shopping-cart-decrease-increase-item-quantity
*/

const reducer = (state, action) => {

  switch(action.type){

    case 'INCREMENT' :

        if (action.type === `INCREMENT`) {
        return Object.assign({}, state, {
          count: state.count + 1,
        })
      }

    break;
    
    case 'ADD_ITEMS_TO_SHOP': // add items to the hash table

      state = {

      		...state,
      		byHash: action.object
      }

    break;

    case 'QTY_UPDATED': // increase or reduce quantity of an item

       Object.keys(state.byHash).map(function(s){

        if(state.byHash[s].id === action.id && action.qty>=0){

           state.byHash[s].qty = action.qty 

        }

      })

      if(action.action==='more'){
        return Object.assign({}, state, {
          count: state.count + 1,
        })
      }
      else{

        if(state.count>0){

            return Object.assign({}, state, {
              count: state.count -1,
            })

        }

      }

    break;
    default:
        return state
    break;

  }

  //console.log( state.count)

  return state
}

const initialState = { count: 0, byId:[], byHash:{} }

const createStore = () => reduxCreateStore(reducer, initialState)
export default createStore